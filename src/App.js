import './App.css';
import pizza from '../public/logo192.png'
function App() {
  return (
    <div className="App">
      <header className="App-header">
      <div className="pt-5">
          <div className="flex-grow-1 w-100 d-flex align-items-center">
            <div style={{ backgroundImage: 'url(' + pizza + ')' }} />
            </div>
          </div>                  
      </header>
    </div>
  );
}

export default App;
